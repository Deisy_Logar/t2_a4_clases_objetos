import scala.collection.SortedMap.Default


class Paciente(private var nombre: String,
                private var primerApe: String,
                private var segundoApe: String,
                private var edad: Int,
                private val fecha: Array[String],
                private val horaRegistro: Array[Int],
                private val nivelBienestar: Array[Byte],
                private val temperatura: Array[Int],
                private val humedad: Array[Int]) {
  
  def this(nombre: String, primerApe: String, segundoApe: String, edad: Int) {
    
    this(nombre, primerApe, segundoApe, edad, null, null, null, null, null)
    
  }
  
  def promedioNivelBienestar(): Unit = {
    
  }
  
  def obtenerTemperaturaMayor(): Unit = {
    
  }
  
  def obtenerTemperaturaMenor(): Unit = {
    
  }
  
  override def toString : String = " === DATOS PACIENTE === " +
                                    "\n Nombre: " + nombre +
                                    "\n Primer Apellido: " + primerApe +
                                    "\n Segundo Apellido: " + segundoApe +
                                    "\n Edad: " + edad
}


object Prueba {
  
  def main(args: Array[String]): Unit = {
    
    println("PACIENTES")
    
    var obj1 = new Paciente("Deisy", "Lozano", "Garcia", 22)
    
    println(obj1)
    
  }
  
}
